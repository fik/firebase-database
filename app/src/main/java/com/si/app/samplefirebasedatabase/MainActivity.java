package com.si.app.samplefirebasedatabase;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.si.app.samplefirebasedatabase.adapter.RecommendedAdapter;
import com.si.app.samplefirebasedatabase.model.RecomendedApps;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private RecyclerView rvRecommended;

    private RecommendedAdapter adapterRecommended;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference("recommended_apps");

        rvRecommended = findViewById(R.id.rv_recommend);

        rvRecommended.setLayoutManager(new LinearLayoutManager(getApplicationContext(),
                LinearLayoutManager.HORIZONTAL,false));
        rvRecommended.getLayoutManager().setAutoMeasureEnabled(true);
        rvRecommended.setNestedScrollingEnabled(false);
        rvRecommended.setHasFixedSize(false);

        adapterRecommended = new RecommendedAdapter(this);

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {

                ArrayList<RecomendedApps> data = new ArrayList<>();
                for (DataSnapshot childSnapshot : dataSnapshot.getChildren()) {
                    RecomendedApps datas = childSnapshot.getValue(RecomendedApps.class);
                    data.add(datas);
                }

                adapterRecommended.setArray(data);
                rvRecommended.setAdapter(adapterRecommended);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        adapterRecommended.onClick(new RecommendedAdapter.onItemClickListener() {
            @Override
            public void onItemClickListener(int position, RecomendedApps recomendedApps) {
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + recomendedApps.getPackage_name())));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + recomendedApps.getPackage_name())));
                }
            }
        });
    }
}
