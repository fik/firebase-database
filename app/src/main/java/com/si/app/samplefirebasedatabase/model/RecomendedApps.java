package com.si.app.samplefirebasedatabase.model;

/**
 * Created by Mochamad Taufik on 26-Dec-19.
 * Email   : thidayat13@gmail.com
 */
public class RecomendedApps {

    private String name;
    private String icon;
    private String description;
    private String rating;
    private String url;
    private String package_name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPackage_name() {
        return package_name;
    }

    public void setPackage_name(String package_name) {
        this.package_name = package_name;
    }
}
