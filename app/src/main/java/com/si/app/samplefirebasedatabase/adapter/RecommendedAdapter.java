package com.si.app.samplefirebasedatabase.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.si.app.samplefirebasedatabase.R;
import com.si.app.samplefirebasedatabase.model.RecomendedApps;

import java.util.List;

/**
 * Created by Mochamad Taufik on 26-Dec-19.
 * Email   : thidayat13@gmail.com
 */

public class RecommendedAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<RecomendedApps> arrlist;

    private TextView tvName, tvDescription, tvRating;

    private static Context context;
    private Activity parentAct;

    private onItemClickListener mListener;
    private onItemLongClickListener mListenerLong;

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater
                .from(parent.getContext()).inflate(R.layout.item_list_recommended, parent, false);
        return new RecyclerView.ViewHolder(view) {
        };
    }

    public void setArray(List<RecomendedApps> dList) {
        this.arrlist = dList;
    }

    public RecommendedAdapter(Context c) {
        context = c;
    }

    public void onClick(onItemClickListener listener) {
        mListener = listener;
    }

    //setListener agar bisa di panggil di activity
    public void onLongClick(onItemLongClickListener listener) {
        mListenerLong = listener;
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {

        tvName            = holder.itemView.findViewById(R.id.name);
        tvDescription     = holder.itemView.findViewById(R.id.description);
        tvRating          = holder.itemView.findViewById(R.id.rating);

        tvName.setText(arrlist.get(position).getName());
        tvDescription.setText(arrlist.get(position).getDescription());
        tvRating.setText(arrlist.get(position).getRating());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //menggunakan listener agar onclick dapat dipanggil di activity
                if (mListener != null)
                    mListener.onItemClickListener(position, arrlist.get(position));
            }
        });

        //onLongClick
        holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                //menggunakan listener agar onclick dapat dipanggil di activity
                if (mListenerLong != null)
                    mListenerLong.onItemLongClickListener(position, arrlist.get(position));

                return true;
            }
        });

    }

    @Override
    public int getItemCount() {
        return arrlist.size();
    }

    public interface onItemClickListener {
        public void onItemClickListener(int position, RecomendedApps recomendedApps);
    }

    //interface listener onLongClick
    public interface onItemLongClickListener {
        public void onItemLongClickListener(int position, RecomendedApps recomendedApps);
    }

}
